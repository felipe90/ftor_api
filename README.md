# FTOR - API (FRAMEWORK DE TESTE ORIENTADO AO REUSO - BACKEND)

## Conteúdo do arquivo 

---------------------------

* Introdução
* Requisitos
* Uso da linguagem Gherkin
* Diretorios do projeto
* Execução dos testes
* Análise dos resultados via relatório surefire-reports
* Inclusão dos testes em pipeline

### Introdução

---------------------------

Projeto de testes de **API**, criado para apoiar o processo de teste em projetos ágeis. O projeto utiliza a seguinte stack:

* Java
* Maven
* Cucumber
* Rest Assured
* JUnit

Quando os testes são executados via comando maven é gerado relatório surefire-report no formato .html

### Requisitos

---------------------------

* Java 21
* Maven
* Eclipse ou Intellij (com plugin do Cucumber)

### Uso da linguagem Gherkin

---------------------------

Nesse projeto, os cenários de teste são escritos em **linguagem natural**, utilizando o **Gherkin**, sendo esta criada para descriçoes de **comportamentos**,
permitindo escrever os cenários utilizando palavras-chave como: **Dado** (pré-condição), **Quando** (ação que o usuário executa), **E** (também uma ação, complementa o cenário) e **Então** (observação das saídas).

Ex:

**Dado** que eu esteja de uma determinada forma
**Quando** realizar determinada ação
**Então** deverá acontecer determinada situação

Dessa maneira, conseguimos escrever nossos cenários, **omitindo detalhes da lógica de programação**, focando assim nos **comportamentos**.

Um dos propósitos de se trabalhar dessa forma é buscar a aplicação da metodologia de desenvolvimento ágil **BDD (Behavior Driven Development)**. Essa metodologia tem o intuito de encorajar a colaboração entre os membros do time, de forma que **PMs/POs, Devs e QAs** consigam trabalhar olhando para o **mesmo artefato vivo** descrito utilizando o Gherkin.

**Observação:** Para escrever nossas features e nossos cenários em Gherkin no projeto, utilizamos o **framework Cucumber**.

### Diretórios do projeto

---------------------------

* **src/test/resources/features** -> Onde armazenamos as features do Cucumber;
* **src/test/resources/json** -> Onde armazenamos os jsons (payloads) que utilizamos nos testes;
* **src/test/java/steps** -> Onde implementamos os steps do Cucumber (Dado, Quando, Então, etc). Cada step de um cenário presente em uma feature Cucumber deve ser implementado em uma classe de step. Para gerar os steps de forma automática, basta executar a feature Cucumber;
* **src/test/java/core** -> Onde implementados métodos que serão utilizados pelos steps;
* **src/test/java/runners** -> Onde encontramos o Runner do Cucumber. Configuraçoes de execução do Cucumber (onde ele irá procurar as features, quais tags serão executadas, etc);
* **src/test/java/hooks** -> Onde encontramos a classe Hooks, responsável por definir o que será realizado antes de cada bateria de teste e a interface Constants, responsável por armazenar constantes relevantes.
* **src/test/utils** -> Onde encontramos e implementamos métodos de apoio (visando a reutilização de código). Dessa forma, por exemplo, não precisamos  reescrever sempre que precisamos, uma função que valide o status code, por exemplo.
* **env** -> Onde armazemos as variáveis de ambiente. Nelas, podemos armazenar dados como rota dos endpoints, secrets, tokens, etc).

### Execução dos testes

---------------------------

* Executamos os testes **via comando maven**. Dessa forma é gerado um relatório no formato html para análise dos detalhes da execução. Para isso, em um terminal, navegue até a raiz do projeto e execute o seguinte comando: **mvn surefire-report: report -Denv=${ambiente}**

### Análise dos resultados via relatório surefire-report

Após a execução dos testes via comando ```mvn surefire-report:report -Denv=hom```, podemos encontrar o relatório .html no seguinte caminho:
* target/site/surefire-report.html

### Inclusão dos testes em pipeline

---------------------------

Podemos incluir a execução dos testes na pipeline do projeto alvo no Gitlab, adicionando o seguinte step do Rest Assured no arquivo **.gitlab-ci.yml**

```
rest_assured:
  stage: test
  image: markhobson/maven-chrome:jdk-21
  script:
    - mkdir qa_api_tests
    - cd qa_api_tests
    - git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com qa_project
    - cd qa-project/
    - mvn surefire-report:report -Denv=hom
    - resErrors=$(grep -oP "Erros:\ \K\d+" target/surefire-reports/runners.RunnersTest.txt)
    - resFailures=$(grep -oP "Failures:\ \K\d+" target/surefire-reports/runners.RunnersTest.txt)
    - echo "Errors:" $resErrors "Failures:" $resFailures
    - if [[ $resErrors != 0 || $resFailures != 0 ]]; then exit 1; fi
  when: delayed
  start_in: 5 minutes
  artifacts:
    when: always
    paths:
      - qa_api_tests/qa-project/target/site/
      - qa_api_tests/qa-project/target/surefire-reports/
    expire_in: 1 week
    reports:
      junit:
        - qa_api_tests/qa-project/target/site/surefire-report.html   
        - qa_api_tests/qa-project/target/surefire-reports/runners.RunnerTest.txt
  environment:
    name: hom
  only: 
    - master
    - main        
```