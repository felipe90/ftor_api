#language: pt

@funcionais
Funcionalidade: Reqres in - Usuários

  Cenário: Listar usuário específico
    Dado que eu faça uma requisição a API que lista um usuário "2" específico
    Então deverá retornar o status code "200"
    E deverá retornar o content-type em json
    E deverá retornar o response diferente de nulo
    E deverá retornar o response para o usuário "2" consultado
    
  Cenário: Realizar Login com sucesso
    Dado que eu faça uma requisição a API de login com e-mail e senha válidos
    Então deverá retornar o status code "200"
    E deverá retornar o content-type em json
    E deverá retornar o response diferente de nulo
    E deverá retornar um token no response  