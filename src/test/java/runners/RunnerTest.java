package runners;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import hooks.Hooks;

@RunWith(Cucumber.class) 
@CucumberOptions(features = "src/test/resources/features/",
				 glue = "steps",
				 tags = "@funcionais",
				 monochrome = false,
                 dryRun = false
				 ) 
public class RunnerTest extends Hooks {
}
