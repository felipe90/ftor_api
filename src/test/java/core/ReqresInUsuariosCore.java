package core;

import org.hamcrest.Matchers;
import hooks.Constants;
import io.restassured.response.Response;
import utils.Utils;

public class ReqresInUsuariosCore {
	
	Utils util = new Utils();
	public static String userDir = System.getProperty("user.dir");
	
	public static Response getEndpointListarUsuarioEspecifico(String usuario) throws Exception {
	    try {
	    	Utils.response = Utils.realizarGetRequest(Constants.URL_ENDPOINT_LISTA_USUARIO_ESPECIFICO + usuario, false);
	    	
		} catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
		return Utils.response;
	}
	
	public static void validarQueRetornouUsuarioCorreto(String usuario) throws Exception {
		try {
			Utils.response.then().body("data.id", Matchers.equalTo(Integer.parseInt(usuario)));
			
		} catch (Exception e) {
			e.printStackTrace()	;
		}
	}
	
	public static Response postEndpointLoginComPayloadValido() throws Exception {
		try {	
			Utils.bearer_token = "xpto1234";
			
			String payload_login = Utils.carregarJson("login_valido");
			
			Utils.response = Utils.realizarPostRequest(Constants.URL_ENDPOINT_LOGIN, payload_login, false);
				
		} catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
		return Utils.response;
	}
	
	public static void validarExistenciaTokenAposLogin() throws Exception {
		try {
			Utils.response.then().body("token", Matchers.notNullValue());
			
		} catch (Exception e) {
			e.printStackTrace()	;
		}
	}
}
