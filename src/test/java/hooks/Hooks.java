package hooks;

import io.github.cdimascio.dotenv.Dotenv;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.BeforeClass;
import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.responseSpecification;
import static org.hamcrest.Matchers.*;

public class Hooks implements Constants {

	public static RequestSpecification reqSpec;
    public static ResponseSpecification resSpec;
    public static Dotenv dotenv;
    
    @BeforeClass
    public static void setUp(){

        String env = System.getProperty("env");

        if(env.matches("hom")) {
            dotenv = Dotenv
                    .configure()
                    .directory("env")
                    .filename(".env.hom")
                    .load();
            baseURI = dotenv.get("API_URL");
        }

        //Configurações da Request
        RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
        reqBuilder.setContentType(CONTENT_TYPE);
        reqSpec = reqBuilder.build();

        //Configurações do Response
        ResponseSpecBuilder resBuilder = new ResponseSpecBuilder();
        resBuilder.expectResponseTime(lessThan(MAX_TIMEOUT));
        resSpec = resBuilder.build();

        //Aplica as configurações de request e response à toda a classe
        requestSpecification = reqSpec;
        responseSpecification = resSpec;

        enableLoggingOfRequestAndResponseIfValidationFails();
    }
}
