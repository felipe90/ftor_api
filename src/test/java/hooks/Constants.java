package hooks;

import io.restassured.http.ContentType;

public interface Constants {
	final ContentType CONTENT_TYPE = ContentType.JSON;
	final Long MAX_TIMEOUT = 3000L;
	final String URL_ENDPOINT_LISTA_USUARIO_ESPECIFICO = "/api/users/";
	final String URL_ENDPOINT_LOGIN = "/api/login";
	String PATH_JSON_FOLDER = "\\src\\test\\resources\\jsons\\";
	String TOKEN_EXPIRADO = "eyRtWErEqerwqRV123fASFfsrqfafasf";
	String TOKEN_INVALIDO = "eyRtWErEqerwqRV123fASFfsrqfa123432";
}