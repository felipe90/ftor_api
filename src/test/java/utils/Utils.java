package utils;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.hamcrest.Matchers;

import hooks.Constants;
import io.restassured.response.Response;

public class Utils {
	
	public static Response response;
	public static String userDir = System.getProperty("user.dir");
	public static String bearer_token;
	
	
	public static void validarStatusCode(String status_code) throws Exception {
		try {
			if(response.contentType().isBlank()) {
				assertEquals("Não retornou o status code esperado. Esperado: " + Integer.parseInt(status_code) + " | Retornado: " + response.statusCode(), Integer.parseInt(status_code), response.statusCode());
			}
			else {
				String message = response.then().extract().path("message");
				assertEquals("Não retornou o status code esperado. Esperado: " + Integer.parseInt(status_code) + " | Retornado: " + response.statusCode() + " | Message: " + message, Integer.parseInt(status_code), response.statusCode());
				if(message != null) {
					System.out.println("Status Code: " + response.statusCode() + " | Message: " + message);
				}
				else {
					System.out.println("Status Code: " + response.statusCode() + " | **Não retornou mensagem**");
				}
			}

		} catch (Exception e) {
			System.out.println("Ocorreu um erro ao tentar verificar o status code: " + e.getMessage());
		}
	}
	
	public static String carregarJson(String json_name) throws IOException {
		String payload = null;

		try {
			String json_path = userDir + Constants.PATH_JSON_FOLDER + json_name + ".json";
			String json_path_replaced = json_path.replace("\\", "//");
			byte[] json=Files.readAllBytes(Paths.get(json_path_replaced));
			payload = new String(json);

			return payload;
		}
		catch (Exception e) {
			System.out.println("Ocorreu um erro ao tentar carregar um arquivo .json de payload" + e.getMessage());
		}
		return  payload;
	}
	
	public static void validarContentType() throws Exception {
		try {
			assertEquals(Constants.CONTENT_TYPE + "; charset=utf-8", response.contentType());
			
		} catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}
	
	public static void validarQueResponseNaoSejaNulo() throws Exception {
		try {
			response.then().assertThat().body(Matchers.notNullValue());
			
		} catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}
	
	public static Response realizarPostRequest(String url_endpoint, String payload, Boolean auth) throws Exception {
		if (auth) {
			 return given()
					   .headers("Authorization","Bearer " + bearer_token)
					   .log().all()
					   .body(payload)	
				   .when()
				   	   .post(url_endpoint);
		}
		else return given()
		               .log().all()
		               .body(payload)	
	                .when()
	                   .post(url_endpoint);	
	}
	
	public static Response realizarGetRequest(String url_endpoint, Boolean auth) throws Exception {
		try {
			if (auth) {
				response = given()
						.headers("Authorization", "Bearer " + bearer_token)
						.log().all()
						.when()
						.get(url_endpoint);
			} else response = given()
					.log().all()
					.when()
					.get(url_endpoint);
		}
		catch (Exception e) {
			verificaConexao(e);
		}
		return response;
	}

	public static Response realizarGetRequestTokenIncorreto(String url_endpoint, String status_token) throws Exception {
		try {
			if (status_token.equals("token_expirado")) {
				response = given()
						.headers("Authorization", "Bearer " + Constants.TOKEN_EXPIRADO)
						.log().all()
					.when()
						.get(url_endpoint);
			} else if (status_token.equals("token_invalido")) {
				response = given()
						.headers("Authorization", "Bearer " + Constants.TOKEN_INVALIDO)
						.log().all()
					.when()
						.get(url_endpoint);
			}
		}
		catch (Exception e) {
			verificaConexao(e);
		}
		return response;
	}
	
	public static String retornaAccessToken() throws Exception {
		bearer_token = response
				.then()
					.log().all()
					.extract().path("access_token");
		
		return bearer_token;
	}

	public static String retornaStringRandomica(int qtd_caracteres) throws Exception {
		String alpha_numeric_string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "0123456789"
				+ "abcdefghijklmnopqrstuvwxyz";

		StringBuilder sb = new StringBuilder(qtd_caracteres);

		for (int i = 0; i < qtd_caracteres; i++) {
			int index = (int)(alpha_numeric_string.length() * Math.random());
			sb.append(alpha_numeric_string.charAt(index));
		}
		return sb.toString();
	}

	public static void validarTipagemDeChaveEspecifica(String key, Class<?> tipo) throws Exception {
		try {
			response.then().assertThat().body(key, Matchers.isA(tipo));
		}
		catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}

	public static void validarQueAtributoNaoSejaNulo(String key) throws Exception {
		try {
			response.then().assertThat().body(key,Matchers.notNullValue());
		}
		catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}

	public static void validarQueResponseSejaVazio() throws Exception {
		try {
			response.then().assertThat().body(Matchers.emptyOrNullString());
		}
		catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}

	public static void validarQueRetornouMensagemEsperada(String mensagem) throws Exception {
		try {
			response.then().assertThat().body("message", Matchers.equalTo(mensagem));
		}
		catch (Exception e) {
			System.out.println("Ocorreu um erro: " + e.getMessage());
		}
	}

	public static void verificaConexao(Exception e) throws Exception {
		try {
			if (e.getMessage().contains("Connection refused")) {
				System.out.println("A API está fora: Connection refused");
				System.out.println("*************************************");
				fail("A API está fora: Connection refused");
			}
		}
		catch (Exception ex) {
			System.out.println("Ocorreu um erro: " + ex.getMessage());
		}
	}
}