package steps;

import core.ReqresInUsuariosCore;
import hooks.Constants;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import utils.Utils;

public class ReqresInUsuariosSteps {
	
	// Dado 
	@Dado("^que eu faça uma requisição a API que lista um usuário \"([^\"]*)\" específico$")
	public void queEuFaçaUmaRequisiçãoAAPIQueListaUmUsuárioEspecífico(String usuario) throws Throwable {
		Utils.realizarGetRequest(Constants.URL_ENDPOINT_LISTA_USUARIO_ESPECIFICO + usuario, false);
	}
	
	@Dado("^que eu faça uma requisição a API de login com e-mail e senha válidos$")
	public void queEuFaçaUmaRequisiçãoAAPIDeLoginComUsuárioESenhaVálidos() throws Throwable {
	    ReqresInUsuariosCore.postEndpointLoginComPayloadValido();
	}
	
    // E
	@E("^deverá retornar o response para o usuário \"([^\"]*)\" consultado$")
	public void deveráRetornarOResponseParaOUsuárioConsultado(String usuario) throws Throwable {
	   ReqresInUsuariosCore.validarQueRetornouUsuarioCorreto(usuario);
	}
	
	@E("^deverá retornar um token no response$")
	public void deveráRetornarUmTokenNoResponse() throws Throwable {
	    ReqresInUsuariosCore.validarExistenciaTokenAposLogin();
	}
}
