package steps;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import utils.Utils;

public class CommonSteps {
	
	// E
	@E("^deverá retornar o content-type em json$")
	public void deveraApresentarContentTypeEmJson() throws Throwable {
		Utils.validarContentType();;
	}
	
	@E("^deverá retornar o response diferente de nulo$")
	public void deveraApresentarConteudoNoResponse() throws Throwable {
		Utils.validarQueResponseNaoSejaNulo();
	}
	
	// Então
    @Então("^deverá retornar o status code \"([^\"]*)\"$")
	public void deveráRetornarOStatusCode(String status_code) throws Throwable {
    	Utils.validarStatusCode(status_code);
	}
}